from bs4 import BeautifulSoup
import urllib

server=r'https://storage.googleapis.com/lun-ua/images-cropped/'
image_name=522508834

name=0
while (name<10000):
    name+=1
    image_name+=1
    fullName = 'photos10000/'+f'{name:05}' + ".jpg"
    link=server+str(image_name)+'.jpg'
    try:
        urllib.request.urlretrieve(link, fullName)
    except:
        name-=1
