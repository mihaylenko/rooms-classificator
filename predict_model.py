import tensorflow as tf
import numpy as np
from os.path import dirname
import os
from urllib.request import urlopen
from PIL import Image
import matplotlib.pyplot as plt

def create_image_tensor():
    image_array_placeholder=tf.placeholder(dtype=tf.int64,shape=[None,None,3],name='image_array')
    image_tensor=tf.expand_dims(tf.cast(tf.image.resize(image_array_placeholder,size=(224,224)),dtype=tf.float32)/255.0,axis=0)
    return image_tensor

def show_image(image_array,image_class):
    imgplot = plt.imshow(image_array)
    plt.title(image_class)
    plt.axis('off')
    plt.show()

classes=[o for o in os.listdir('photos10000') 
                    if os.path.isdir(os.path.join('photos10000',o))]
classes.sort()
classes.remove('other')
classes=dict(zip(range(len(classes)),classes))

image_link=r'https://storage.googleapis.com/lun-ua/images-cropped/500370671.jpg'

image_tensor=create_image_tensor()

with tf.Session() as sess:
    model=tf.keras.models.load_model(dirname(__file__)+'\\logs\\model_trained.h5')


    image_array=np.array(Image.open(urlopen(image_link)))

    result=model.predict(x=image_tensor.eval(feed_dict={'image_array:0':image_array}),
                    steps=1)
    image_class=classes[np.argmax(result)]
    

    show_image(image_array,image_class)