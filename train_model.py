import tensorflow as tf
import os
from random import shuffle

import matplotlib.pyplot as plt
import numpy as np
import sklearn.metrics
import io

def delete_old_summaries():
    accuracy_dir='logs/accuracy'
    cm_dir='logs/cm'
    file_name='events.out.tfevents'
    filelist = [os.path.join(accuracy_dir, f) for f in os.listdir(accuracy_dir) if file_name in f] + \
                [os.path.join(cm_dir, f) for f in os.listdir(cm_dir) if file_name in f]
    for f in filelist:
        os.remove(f)

def get_all_images():
    dirs=[o for o in os.listdir('photos10000') 
                    if os.path.isdir(os.path.join('photos10000',o))]
    dirs.sort()
    dirs.remove('other')
    image_lib=[]
    for dir in dirs:
        mypath='photos10000/'+dir
        image_lib.append([{'image':os.path.join(mypath,f),'class':dir} for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))])
    class_names=dirs
    return image_lib,class_names

def get_train_and_val_lists():
    image_lib,class_names=get_all_images()
    image_lib=[[(image['image'],image['class'])  for image in image_class] for image_class in image_lib]
    
    splited_list=[split_list(x) for x in image_lib]
    train_list,val_list= [x[0] for x in splited_list], [x[1] for x in splited_list]
    train_list= [image for image_class in train_list for image in image_class]
    val_list= [image for image_class in val_list for image in image_class]

    shuffle(train_list)
    shuffle(val_list)

    return train_list, val_list, class_names

def split_list(list__):
    train_list=list__[:int(len(list__)*0.80)]
    val_list=list__[int(len(list__)*0.80):]
    return  train_list,val_list

def apply_settings_to_dataset(list_of_images,table,class_names_len,batch_size,is_training=True):
    dataset=tf.data.Dataset.from_tensor_slices(list_of_images)
    dataset=dataset.map(lambda x:(tf.cast(tf.image.resize(tf.image.decode_jpeg(tf.read_file(x[0]),channels=3),size=(224,224)),dtype=tf.float32)/255.0,
                    tf.one_hot(table.lookup(x[1]),depth=class_names_len)),num_parallel_calls=1)
    if is_training==True:
        dataset=dataset.shuffle(300)
    dataset=dataset.batch(batch_size,drop_remainder=True).prefetch(1)
    dataset=dataset.repeat()
    return dataset

def create_datasets(batch_size=5):

    train_list, val_list, class_names=get_train_and_val_lists()
    table=tf.contrib.lookup.index_table_from_tensor(mapping=tf.constant(class_names), num_oov_buckets=1, default_value=-1)

    train_set=apply_settings_to_dataset(train_list,table,len(class_names),batch_size,is_training=True)

    val_set=apply_settings_to_dataset(val_list,table,len(class_names),batch_size,is_training=False)

    train_steps_per_epoch=len(train_list)//batch_size
    val_steps_per_epoch=len(val_list)//batch_size

    val_labels=[class_names.index(example[1]) for example in val_list[:val_steps_per_epoch*batch_size]]

    return train_set,val_set,train_steps_per_epoch,val_steps_per_epoch,class_names,val_labels

def plot_to_image_buffer(figure):
    """Converts the matplotlib plot specified by 'figure' to a PNG image and
    returns it. The supplied figure is closed and inaccessible after this call."""
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # Closing the figure prevents it from being displayed directly inside
    # the notebook.
    plt.close(figure)
    buf.seek(0)
    return buf.getvalue()

def plot_confusion_matrix(cm, class_names):
    """
    Returns a matplotlib figure containing the plotted confusion matrix.    
    Args:
      cm (array, shape = [n, n]): a confusion matrix of integer classes
      class_names (array, shape = [n]): String names of the integer classes
    """
    figure = plt.figure(figsize=(8, 8))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("Confusion matrix")
    #plt.colorbar()
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names, rotation=45)
    plt.yticks(tick_marks, class_names) 
    # Use white text if squares are dark; otherwise black.
    threshold = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            plt.text(j, i, cm[i, j], horizontalalignment="center", 
                     color="white" if cm[i, j] > threshold else "black")   
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return figure

def create_cm_summary():
    image_buffer=tf.placeholder(dtype=tf.string,shape=[],name='image_buffer')
    cm_image = tf.expand_dims(tf.image.decode_png(image_buffer, channels=3), 0)
    # Log the confusion matrix as an image summary.
    image_summary=tf.summary.image("Confusion Matrix", cm_image)
    return image_summary

def get_confusion_matrix(val_labels, val_pred):
    cm = sklearn.metrics.confusion_matrix(val_labels, val_pred)#val_labels хранятся в dataset
    # Normalize the confusion matrix.
    cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)
    return cm

def log_confusion_matrix(epoch, logs):
    val_pred_raw = model.predict(x=val_set,batch_size=5,steps=val_steps_per_epoch)
    val_pred = np.argmax(val_pred_raw, axis=1)  
    cm=get_confusion_matrix(val_labels, val_pred)
    figure=plot_confusion_matrix(cm, class_names=class_names)
    image_buffer=plot_to_image_buffer(figure)
    summary_writer.add_summary(image_summary.eval(feed_dict={'image_buffer:0':image_buffer}),epoch)
        

train_set,val_set,train_steps_per_epoch, \
        val_steps_per_epoch, class_names, val_labels=create_datasets()

summary_writer = tf.summary.FileWriter('logs/cm')
image_summary=create_cm_summary()
cm_callback = tf.keras.callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix)

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir='logs/accuracy',write_graph=False)
cp_callback = tf.keras.callbacks.ModelCheckpoint('logs/model_trained.h5', verbose=1)


delete_old_summaries()

with tf.Session() as sess:

    tf.tables_initializer().run()


    model=tf.keras.models.load_model('logs/model.h5')
       
    model.fit(x=train_set,steps_per_epoch=train_steps_per_epoch,epochs=7,
                    validation_data=val_set,validation_steps=val_steps_per_epoch,
                    callbacks=[tensorboard_callback, cp_callback,cm_callback])
