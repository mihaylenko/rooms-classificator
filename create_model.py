import tensorflow as tf
from os.path import dirname

def create_model():
    base_model = tf.keras.applications.MobileNetV2(include_top=False)
    model = tf.keras.models.Sequential()
    model.add(base_model)
    model.add(tf.keras.layers.GlobalAveragePooling2D())
    model.add(tf.keras.layers.Dense(9,'softmax',name='output'))
    
    
    for layer in model.layers[:-2]:
        layer.trainable = False
    for layer in model.layers[-2:]:
        layer.trainable = True
    
    model.compile(loss='categorical_crossentropy',#tf.keras.losses.CategoricalCrossentropy() в чем разница
                    optimizer=tf.keras.optimizers.Adam(lr=1e-4),
                    metrics=['acc']) 

    model.save(dirname(__file__)+'\\logs\\model.h5')




def save_model():
    tf.train.export_meta_graph(filename=dirname(
       __file__)+'\\logs\\tf_graph.meta', clear_devices=True)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver(tf.global_variables(),  # Saves also various optimizers
                               max_to_keep=1000,
                               # filename='settings\\variables',
                               name='saver')
        saver.save(sess, dirname(__file__)+'\\logs\\variables',
                   latest_filename='variables_ckpt',
                   write_meta_graph=False)
        tf.summary.FileWriter(dirname(__file__)+'/logs/model', sess.graph)


create_model()
#save_model()
with tf.Session() as sess:
    tf.summary.FileWriter(dirname(__file__)+'/logs/model', sess.graph)